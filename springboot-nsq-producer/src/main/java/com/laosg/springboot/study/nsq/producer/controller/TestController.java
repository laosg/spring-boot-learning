package com.laosg.springboot.study.nsq.producer.controller;

import cn.hutool.json.JSONUtil;
import com.laosg.springboot.study.nsq.producer.config.properties.NsqProperties;
import com.laosg.springboot.study.nsq.producer.service.MessageService;
import com.laosg.springboot.study.nsq.producer.service.impl.ConsumerService;
import com.sproutsocial.nsq.Subscriber;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by kaimin on 29/4/2019.
 * time : 09:57
 */
@RestController
@RequestMapping("test")
public class TestController {
    @Autowired
    private NsqProperties nsqProperties;
    @Autowired
    private MessageService messageService;

//    @Autowired
    private Subscriber subscriber;

    @Autowired
    private ConsumerService consumerService;

    @GetMapping
    public String hello(){
        return JSONUtil.toJsonStr(nsqProperties);
    }

    @GetMapping("/{msg}")
    public String send(@PathVariable("msg") String msg) {
        return messageService.sentMessage(msg);
    }


    @GetMapping("/send/{msg}")
    public String send2(@PathVariable("msg") String msg) {
        return messageService.sentMessage2(msg);
    }
    @GetMapping("/con3")
    public String consumer3() {
        subscriber.subscribe(nsqProperties.getTopic(),"Channel1",consumerService);
        return "消费3成功";
    }

    @GetMapping("/com")
    public String consumer() {
        messageService.consume();
        return "消费成功";
    }
    @GetMapping("/com2")
    public String consumer2() {
        messageService.consume2();
        return "消费2成功";
    }



}
