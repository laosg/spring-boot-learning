package com.laosg.springboot.study.nsq.producer.config.properties;

import lombok.Data;

import java.util.List;

/**
 * Created by kaimin on 29/4/2019.
 * time : 10:39
 */
@Data
public class NsqBaseProperties {
    private List<String> addresses;
    private List<Integer> ports;
}
