package com.laosg.springboot.study.nsq.producer.config.properties;

import lombok.Data;

/**
 * Created by kaimin on 29/4/2019.
 * time : 10:40
 */
@Data
public class NsqlookupdProperties extends NsqBaseProperties{
}
