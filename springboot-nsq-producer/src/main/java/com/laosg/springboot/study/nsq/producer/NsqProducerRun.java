package com.laosg.springboot.study.nsq.producer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * Created by kaimin on 29/4/2019.
 * time : 09:32
 */
@SpringBootApplication
@EnableConfigurationProperties
public class NsqProducerRun {
    public static void main(String[] args) {
        SpringApplication.run(NsqProducerRun.class,args);
    }

}
