package org.spring.springboot.config;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.MarkerManager;
import org.apache.logging.log4j.ThreadContext;
import org.apache.logging.log4j.message.MapMessage;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * @author kaimin
 * @Date 2017/4/19
 */
public class LogUtil {

    private Logger logger;

    private ObjectMapper mapper;
    private LogUtil(Logger logger) {
        this.logger = logger;
    }

    public static LogUtil getLogger(Class<?> clazz) {

        Logger logger = LogManager.getLogger(clazz);

        return new LogUtil(logger);
    }

    public ObjectMapper getMapper() {
        return new ObjectMapper();
    }



    /**
     * 操作日志
     */
    public void operation(BusinessModule businessModule, AcitonType actionType, String operation, String description,
                          Map<String, String> options) {

//        User user = UserContext.get();
        MapMessage map = new MapMessage();
        map.put("userId", String.valueOf(1));
        map.put("role", "");
        map.put("logType", "bops-operations");
        map.put("actionType", actionType.name());
        map.put("operation", operation);
        map.put("businessType", businessModule.name());
        map.put("description", description);
        map.put("authority", "public");
//        map.put("options", JacksonUtil.nonEmptyMapper().toJson(options));
        logger.info(MarkerManager.getMarker("OPERATION"), map);
    }

    /**
     * 操作日志,无map
     */
//    public void operation(BusinessModule businessModule, AcitonType actionType, String operation, String description) {
//
//        User user = UserContext.get();
//        MapMessage map = new MapMessage();
//        map.put("userId", String.valueOf(user.getUserId()));
//        map.put("role", "");
//        map.put("logType", "bops-operations");
//        map.put("actionType", actionType.name());
//        map.put("operation", operation);
//        map.put("businessType", businessModule.name());
//        map.put("description", description);
//        map.put("authority", user.getName());
//        Map<String, Object> options = new ConcurrentHashMap<>();
//        options.put("name",user.getName());
//        map.put("options", JacksonUtil.nonEmptyMapper().toJson(options));
//        logger.info(MarkerManager.getMarker("OPERATION"), map);
//    }
    /**
     * 操作日志,无map和desciption
     */
    public void operation(BusinessModule businessModule, AcitonType actionType, String operation) {

//        User user = UserContext.get();
        MapMessage map = new MapMessage();
        map.put("userId", String.valueOf(1));
        map.put("role", "");
        map.put("logType", "bops-operations");
        map.put("actionType", actionType.name());
        ThreadContext.put("actionType",actionType.name());
        map.put("operation", operation);
        map.put("businessType", businessModule.name());
        map.put("authority", "public");

        try {
            logger.info(MarkerManager.getMarker("OPERATION"), getMapper().writeValueAsString(map.getData()));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }


    public void warn(String s) {

        logger.warn(s);
    }

    public void warn(String s, Exception e) {

        logger.warn(s, e);
    }

    public void error(String s, Exception e) {

        logger.error(s, e);
    }

    public void error(String s) {

        logger.error(s);
    }

    public void info(String s) {

        logger.info(s);
    }

    public enum BusinessModule {

        CORPORATION, CONTRACT,CONTRACT_TEMPLATE, PRODUCT, TRADE, SETTLEMENT, BILL, FINANCE,SASS,USER


    }
    public enum AcitonType {
        ADD,VIEW,EDIT,DEL,CLOSE,CANCLE,CHEACK
    }

}
