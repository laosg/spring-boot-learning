package org.spring.springboot.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.pool.DruidDataSourceFactory;
import org.apache.ibatis.datasource.DataSourceFactory;
import org.apache.log4j.jdbc.JDBCAppender;
import org.apache.log4j.spi.ErrorCode;
import org.spring.springboot.Application;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by kaimin on 20/2/2019.
 * time : 09:17
 */
@Component
public class Log4j2ConnectionFactory extends JDBCAppender{
    /* Druid数据源 */
    private DruidDataSource dataSource;
    private static Log4j2ConnectionFactory connectionFactory;
    public Log4j2ConnectionFactory() {
        super();
    }

    @Override
    protected void closeConnection(Connection con) {
        try {
            /* 如果数据库连接对象不为空和没有被关闭的话，关闭数据库连接 */
            if (con != null && !con.isClosed())
                con.close();
        } catch (SQLException e) {
            errorHandler.error("Error closing MyJDBCAppender.closeConnection() 's connection", e, ErrorCode.GENERIC_FAILURE);
        }
    }

    @Override
    protected Connection getConnection() throws SQLException {
        String className = "com.mysql.jdbc.Driver";
        String connUrl = "jdbc:mysql://hdm-mysql:3306/test?characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=GMT%2B8&allowMultiQueries=true";
        String uname = "root";
        String psw = "zQitaKGvR3mFU5mI";
        Properties result = new Properties();
        result.put("driverClassName", className);
        result.put("url", connUrl);
        result.put("username", uname);
        result.put("password", psw);

        /* 其他配置 自然你也可以自己写property 然后获取set */
        result.put("maxActive", "30");
        result.put("minIdle", "3");

        try {
            dataSource = (DruidDataSource) DruidDataSourceFactory.createDataSource(result);
        } catch (Exception e) {
            /* Druid数据库源对象产生失败后，取消初始化 */
            try {
                uninitialize();
            } catch (Exception e2) {
            }
        }

        return dataSource.getConnection();
    }
    /* 取消初始化 */
    public void uninitialize() {
        try {
            if (dataSource != null)
                dataSource.close();
        } catch (Exception e) {
        } finally {
            super.close();
        }
    }
    public static Connection getDataSourceConnection() throws SQLException {
        if (connectionFactory==null){
            connectionFactory = new Log4j2ConnectionFactory();
        }
        return connectionFactory.getConnection();
    }




}
