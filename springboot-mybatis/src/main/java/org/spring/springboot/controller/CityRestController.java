package org.spring.springboot.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.spring.springboot.config.LogUtil;
import org.spring.springboot.domain.City;
import org.spring.springboot.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by bysocket on 07/02/2017.
 */
@RestController
public class CityRestController {

    private static Logger logger = LogManager.getLogger(CityRestController.class);

    @Autowired
    private CityService cityService;

    protected static final LogUtil logger1 = LogUtil.getLogger(CityRestController.class);

    @RequestMapping(value = "/api/city", method = RequestMethod.GET)
    public City findOneCity(@RequestParam(value = "cityName", required = true) String cityName) {
        logger.info("城市名称:"+cityName);
        logger.error("城市名称:"+cityName);
        logger1.operation(LogUtil.BusinessModule.BILL, LogUtil.AcitonType.ADD,"查看");
        return cityService.findCityByName(cityName);
    }

}
