package com.laosg.springboot.study.rocketmq.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by kaimin on 5/5/2019.
 * time : 15:11
 */
@SpringBootApplication
public class Consumer2AppRun {
    public static void main(String[] args) {
        SpringApplication.run(Consumer2AppRun.class,args);

    }
}
