package com.laosg.springboot.study.nsq.consumer.service;

/**
 * Created by kaimin on 29/4/2019.
 * time : 10:15
 */
public interface MessageService {
    String sentMessage(String body);

    String sentMessage2(String body);

    void consume();

    void consume2();
}
