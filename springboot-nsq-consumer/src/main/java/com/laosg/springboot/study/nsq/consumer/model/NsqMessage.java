package com.laosg.springboot.study.nsq.consumer.model;

import lombok.Data;
import lombok.ToString;

/**
 * Created by kaimin on 29/4/2019.
 * time : 10:08
 * nsq 传递的消息
 */
@Data
@ToString
public class NsqMessage {

    private Long id;
//    相当于nsq消费者的channel名称
    private String action;
    //消息内容
    private String body;


}
