package com.laosg.springboot.study.nsq.consumer.config.properties;

import lombok.Data;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by kaimin on 29/4/2019.
 * time : 09:43
 */
@Data
@ConfigurationProperties(prefix = "nsq")
@Component
@ToString
public class NsqProperties {
    private String topic;
    private NsqdProperties nsqd;
    private NsqlookupdProperties nsqlookup;



}
