package com.laosg.springboot.study.rocketmq.provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by kaimin on 5/5/2019.
 * time : 14:42
 */
@SpringBootApplication
public class ProviderAppRun {
    public static void main(String[] args) {
        SpringApplication.run(ProviderAppRun.class,args);
    }
}
